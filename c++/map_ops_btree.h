#pragma once
#define get_mid(a, b) (int)ceil((a+b)/2.0)

template<class EntryT>
struct map_ops: sequence_ops<b_tree_node<EntryT>> {
  using Entry = EntryT;
  using Seq = sequence_ops<b_tree_node<EntryT>>;
  using tnode = typename Seq::tnode;
  using ET = typename Seq::ET;
  using AT = typename Seq::AT;
  using K = typename Entry::key_t;
  using V = typename Entry::val_t;
  using rt_nodes = typename Seq::rt_node;
  using RT = typename Seq::RT;  

  static bool comp(K a, K b) { return Entry::comp(a, b);}

  static typename Entry::key_t get_key (tnode* b, size_t mid) {
    return tnode::get_key(b->nodes[mid]);
  }

  //static 

  inline static int get_left_index(size_t low, size_t mid, size_t high) {
    if (low == high) return -1;
    return ceil((low + mid-1)/2.0);
  }

  inline static int get_right_index (size_t low, size_t mid, size_t high) {
    if (mid == high) return -1;
    return ceil((mid+1 + high)/2.0);
  }

  static RT bsearch_insert_leaf (tnode* b, const ET& e) {
    int low = 0;
    int high = b->get_size() - 1;
    int mid;
    int cur = 0;
    RT ret_val;

    while (low <= high) { 
      mid = get_mid(low, high);
      switch (tnode::compare(get_key(b, mid), tnode::get_key(e))) {
        case 1 :
          low = mid + 1;
          break;
        case 0 :
          b->nodes[mid] = e;
          b->update();
          ret_val.first = false;
          ret_val.second.first = b;
          return ret_val;
        case -1 :
          high = mid - 1;
          break;
      }
    }

    cur--;
    ret_val = Seq::insert_node(b, e, ret_val, low);
    if (!ret_val.first) {
      ret_val.second.first = b;
    }
    return ret_val;
  }

  static AT get_total_partial_sum (tnode* b) {
    return b->partial_sum[get_mid(0, b->get_size() - 1)];
  }

  static RT bsearch_insert(tnode* b, const ET& e) {
    int low = 0;
    int high = b->get_size()-1;
    if (b->is_leaf) {return bsearch_insert_leaf(b, e);}
    int mids[20];
    int l_index[20];
    int r_index[20];
    int cur=0;
    int mid;
    bool is_right = false;
    bool can_insert = true;

    while (low <= high) {
      mid = get_mid(low, high);
      mids[cur] = mid;
      l_index[cur] = get_left_index(low, mid, high);
      r_index[cur] = get_right_index(low, mid, high);
      cur++;
      switch (tnode::compare(get_key(b, mid), tnode::get_key(e))) {
        case 1 :
          low = mid + 1;
          break;
        case 0 :
          b->nodes[mid] = e;
          high = -1;
          can_insert = false;
          break;
        case -1 :
          high = mid - 1;
          break;
      }
    }
    cur--;
    RT result;
    if (can_insert)
      result = bsearch_insert(b->children[low], e);
    if (!result.first) {
      result.second.first = b;
      while (cur >= 0) {
        b->update(mids[cur], l_index[cur], r_index[cur]);
        cur--;
      }
      b->tree_size = b->t_sizes[get_mid(0, b->get_size() - 1)];
      return result;
    } else {
      return Seq::insert_node(b, result.second.entry_key, result, low);
    }
  }

  inline static tnode*  make_tree_node () {
    return Seq::make_tree_node();
  }

  static tnode* insert(tnode* b, const ET& e) {
    RT r = bsearch_insert(b, e);
    if (r.first) {
      return Seq::create_root(r);
    }
    return r.second.first;
  }

  static void print (tnode* root) {
    if (!root->is_leaf) {
      for (int i = 0; i < root->get_size(); i++) {
        print(root->children[i]);
        std::cout<<root->nodes[i].first << " " << root->nodes[i].first << std::endl;
      }
      print(root->children[root->get_size()]);
    } else {
      for (int i = 0; i < root->get_size(); i++) {
        std::cout<<root->nodes[i].first << " " << root->nodes[i].first << std::endl;
      }
    }
  }

  static AT aug_right (tnode* b, const key_t& k, int low, int high) {
    if (low < 0 || high >= (int)(b->get_size())) {
      return Entry::get_empty();
    }
    if (low > high) {
      return aug_right (b->get_child(low), k);
    } else {
      int mid = get_mid (low, high);
      if (!tnode::comp (get_key(b, mid), k)) {
        AT aval = Entry::from_entry(b->nodes[mid]);
        aval = Entry::combine(aug_right(b, k, low, mid-1), aval);
        return Entry::combine(aval, b->get_partial_sum(mid + 1, high));
      } else {
        return aug_right(b, k, mid+1, high);        
      }
    }
  }

  static AT aug_right (tnode* b, const key_t& k) {
    if (!b) { 
      return Entry::get_empty();
    } else {
      return aug_right (b, k, 0, b->get_size()-1 );
    }
  }

  static AT aug_left (tnode* b, const key_t& k, int low, int high) {
    if (low < 0 || high > (int)(b->get_size() -1)) {
      return Entry::get_empty();
    }
    if (low > high) {
      return aug_left(b->get_child(low), k);
    } else {
      int mid = get_mid(low, high);
      if (!tnode::comp(k, get_key(b, mid))) {
        AT aval = Entry::from_entry(b->nodes[mid]);
        aval = Entry::combine(b->get_partial_sum(low, mid-1), aval);
        return Entry::combine(aval, aug_left(b, k, mid+1, high));
      } else {
        return aug_left(b, k, low, mid-1);
      }
    }
  }

  static AT aug_left (tnode* b, const key_t& k) {
    if (!b) {
      return Entry::get_empty();
    } else {
      return aug_left(b, k, 0, b->get_size() -1);
    } 
  }

  // Need to handle the case of equality of keys
  static AT aug_range (tnode* b, const key_t& k1, const key_t& k2, int low, int high) {
    if (low > high) {
      return aug_range(b->children[low], k1, k2);
    } else {
      int mid = get_mid (low, high);
      if (!tnode::comp(k2, get_key(b, mid))) {
        if (!tnode::comp(get_key(b, mid), k1)) {
          AT a = tnode::from_entry(b->nodes[mid]);
          a = tnode::combine (aug_right (b, k1, low, mid-1), a);
          a = tnode::combine (a, aug_left (b, k2, mid+1, high));
          return a;
        } else {
          return aug_range(b, k1, k2, mid+1, high);
        }
      } else {
        return aug_range(b, k1, k2, low, mid-1);
      }
    }
  }

  static AT aug_range (tnode* b, const key_t& k1, const key_t& k2) {
    if (!b) {
      return Entry::get_empty();
    } 
    return aug_range (b, k1, k2, 0, b->get_size() - 1);
  }

  static maybe<ET> find (const key_t& k, tnode* root) {
    tnode* temp = root;
    while (temp != NULL) {
      int low = 0;
      int high = temp->get_size()-1;
      int mid;
      while ( low <= high) {
        mid = get_mid(low, high);
        switch (tnode::compare(get_key(temp, mid), k)) {
          case 1 :
            low = mid + 1;
            break;
          case 0 :
            return maybe<ET>(temp->nodes[mid], true);
            break;
          case -1 :
            high = mid - 1;
            break;
        }
      }
      temp = temp->children[low];
    }
    return maybe<ET>();
  } 
};
