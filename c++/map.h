#pragma once

template<typename _Entry>
struct  map_ {
  public:
  using Entry = _Entry;
  using Tree = map_ops<Entry>;
  using node = typename Tree::node;
  using tnode = typename Tree::tnode;
  using E = typename Entry::ET;
  using K = typename Entry::key_t;
  using V = typename Entry::val_t;
  using M = map_;

  tnode* root;

  // empty constructor
  map_() : root(NULL) { /*GC::init(); */}
  // perhaps remove this one
  map_(bool x) : root(NULL) {  }

  // copy constructor, increment reference count
  map_(const M& m) {//if (root != NULL) cout << "copy: " << (m.root)->ref_cnt <<     endl;
    root = m.root; //GC::increment(root);
  }

  // move constructor, clear the source, leave reference count as is
  /*map_(M&& m) { //cout << "move constructor: " <<endl;
    root = m.root; m.root = NULL;}*/

  map_(tnode* t): root(t) {}

  // copy assignment, clear target, increment reference count, 
  M& operator = (const M& m) {
    if (this != &m) {
      //node* tmp = root;
      clear();
      root = m.root; //GC::increment(root);
      //if (GC::initialized()) GC::decrement_recursive(tmp);
    }
    return *this;
  }

  void clear() {
    tnode* t = root;
    __sync_bool_compare_and_swap(&(this->root), t, NULL);
  }

  void insert (const E& p) {
    auto replace = [] (const V& a, const V& b) { return b;};
    tnode* new_root = Tree::insert(this->root, p, replace);
    this->root = new_root;
  }

  tnode* get_root() {tnode* t = root; root = NULL; return t;}

};
