#pragma once
//#include "pbbslib/list_allocator.h"
#include "pbbslib/alloc.h"

using node_size_t = unsigned int;
//using node_size_t = size_t;

// *******************************************
//   BASIC NODE
// *******************************************

template<class _ET>
struct basic_node {
  using ET = _ET;

  struct node{
    ET entry;
    node_size_t s;

    node(ET& entry): entry(entry) {}
  };
  
  using allocator = pbbs::type_allocator<node>;
  //using allocator = list_allocator<node>;

  static node_size_t size(node* a) {
    return (a == NULL) ? 0 : a->s;
  }

  static node* make_node(ET e) {
    node *o = allocator::alloc();
    pbbs::assign_uninitialized(o->entry,e);
    return o;
  }

  static node* single(ET e) {
    node* r = new node(e);
    r->s = 1;
    return r;
  }

  static node single1 (ET e) {
    node r = node(e);
    return r;
  }

  static void free_node(node* a) {
	(a->entry).~ET();
    allocator::free(a);
  }

  static node* empty() {return NULL;}
  inline static ET& get_entry(node *a) {return a->entry;}
  inline static ET* get_entry_p(node *a) {return &(a->entry);}
  static void set_entry(node *a, ET e) {a->entry = e;}
};

//template <class E>
//using node_type = basic_node<weight_balanced_tree, E>;
