template<class Node>
struct b_tree_node {
  public:
    // holds the number of keys in the BTree node
    using AT = typename Node::aug_t;
    using ET = typename Node::entry_t;
    using K = typename Node::key_t;
    using V = typename Node::val_t;
    using Element = std::pair<ET, AT>;
    using tnode = b_tree_node;

    static const size_t capacity = 8;

    size_t size;
    size_t tree_size;
    bool is_leaf;
    ET nodes[capacity];
    b_tree_node<Node>* children[capacity+1]; 
    AT partial_sum[capacity]; 
    size_t t_sizes[capacity];

    b_tree_node(): nodes(), children() {
      this->size = 0;
      this->is_leaf = true;
      this->tree_size = 0;
    }

    ~b_tree_node() {
      delete children;
    }

    //static ET& get_entry(const Element& a) {return a.first;}

    static inline int compare (const key_t& k1, const key_t& k2) {
      if (comp (k1, k2)) {
        return 1;
      } else if (comp (k2, k1)) {
        return -1;
      }
      return 0;
    }

    static inline bool comp (const key_t& k1, const key_t& k2) {
      return Node::comp(k1, k2);
    }

    static inline K get_key (const Element& e) {
      return e.first.first;
    }

    static inline K get_key(const ET& entry) {
      return Node::get_key(entry);
    }

    static inline AT combine (const AT& a, const AT& b) {
      return Node::combine(a, b);
    }

    static inline AT from_entry (const ET& entry) {
      return Node::from_entry(entry);
    }
    
    static inline AT get_empty () {
      return Node::get_empty();
    }

    static inline int get_mid (int low, int high) {
      if (high < 0) return 0;
      return ceil ((low + high)/2.0);
    }
    
    AT& get_partial_sum () {
      size_t mid = ceil((size-1) /2.0);
      return this->partial_sum[mid];
    }

    AT& get_partial_sum (int index) {
      if (index < 0 || index >= (int)size) {
        return Node::get_empty();
      } else {
        return this->partial_sum[index];
      }
    }

    AT get_partial_sum (int low, int high) {
      if (low < 0 || high >= (int)size) {
        return Node::get_empty();
      }
      if (low > high) {
        return is_leaf? Node::get_empty(): get_child(low)->get_partial_sum();
      } else {
        return this->partial_sum[get_mid(low, high)];
      }
    }

    void validate_tree_size () {
      this->tree_size = this->t_sizes[get_mid(0, size-1)];
    }

    size_t get_tree_size (int index) {
      if (index < 0 || index >= (int)size) {
        return 0;
      } else {
        return this->t_sizes[index];
      }
    }

    size_t get_tree_size () {
      return this->tree_size;
    }

    size_t update_tree_size (int mid, int l_index, int r_index) {
      int s = 1;
      if (l_index != -1) {
        s = (t_sizes)[l_index] + s;
      } else if (children[mid]) {
        s = (children)[mid]->get_tree_size() + s;
      }
      if (r_index != -1) {
        s = s + (t_sizes)[r_index];
      } else if (children[mid+1]) {
        s = s + (children)[mid+1]->get_tree_size();
      }
      this->t_sizes[mid] = s;
      return s;
    }

    size_t update_tree_size () {
      this->tree_size = update_tree_size(0, size-1);
      return this->tree_size;
    }

    size_t update_tree_size(size_t low, size_t high) {
      size_t aval = 0;
      size_t mid = ceil((low + high)/2.0);
      if (low > high || low < 0 || high >= size) {
        return 0;
      }
      if (low == high) {
        aval = 1;
        if (this->children[high])
          aval = this->children[high]->get_tree_size() + aval;
        if (this->children[high + 1])
          aval = aval + this->children[high + 1]->get_tree_size();
        this->t_sizes[low] = aval;
        return aval;
      } else if (low == high-1) {
        aval = update_tree_size(low, low) + 1;
        if (this->children[high + 1])
          aval = aval + (this->children[high + 1])->get_tree_size();
        this->t_sizes[high] = aval;
        return aval;
      } else {
        this->t_sizes[mid] = update_tree_size(low, mid-1) + 1 + update_tree_size(mid+1, high);
        return this->t_sizes[mid];
      }
    }

    void update_partial_sum (size_t mid, int l_index, int r_index) {
        AT aval = Node::from_entry((this->nodes)[mid]);
        if (l_index != -1) {
          aval = Node::combine((partial_sum)[l_index], aval);
        } else if (children[mid]) {
          aval = Node::combine(children[mid]->get_partial_sum(), aval);
        } else {
          aval = Node::combine(Node::get_empty(), aval);
        }
        if (r_index != -1) {
          aval = Node::combine(aval, (partial_sum)[r_index]);
        } else if (children[mid+1]) {
          aval = Node::combine(aval, (children)[mid+1]->get_partial_sum());
        } else {
          aval = Node::combine(aval, Node::get_empty());
        }
        this->partial_sum[mid] = aval;
    }

    void update_partial_sum () {
      update_partial_sum(0, size-1);
    }

    AT update_partial_sum(size_t low, size_t high) {
      size_t mid = ceil((low + high)/2.0);
      if (low > high || low < 0 || high >= size) {
        return Node::get_empty();
      }
      if (low == high) {
        AT aval = Node::from_entry(this->nodes[mid]);
        if (this->children[mid]) {
          aval = Node::combine(aval, (this->children[mid])->get_partial_sum());
        } else {
          aval = Node::combine(Node::get_empty(), aval);
        }
        if (this->children[mid+1]) {
          aval = Node::combine(aval, (this->children[mid + 1])->get_partial_sum());
        } else {
          aval = Node::combine(aval, Node::get_empty());

        }
        this->partial_sum[low] = aval;
        return aval;
      } else if (low == high-1) {
        AT aval = Node::from_entry(this->nodes[high]);
        aval = Node::combine(update_partial_sum(low, low), aval);
        if (this->children[high + 1])
          aval = Node::combine(aval, (this->children[high + 1])->get_partial_sum());
        this->partial_sum[high] = aval;
        return aval;
      } else {
        AT aval = Node::from_entry(this->nodes[mid]);
        aval = Node::combine(update_partial_sum(low, mid-1), aval); 
        aval = Node::combine(aval, update_partial_sum(mid+1, high));
        this->partial_sum[mid] = aval;
        return aval;
      }
    }

    void update (size_t mid, int l_index, int r_index) {
      this->update_partial_sum(mid, l_index, r_index);
      this->update_tree_size(mid, l_index, r_index);
    }

    void update () {
      this->update_partial_sum();
      this->update_tree_size();
    }

    ET& get_node (int index) {
      if (index < 0 || index >= size) {
        throw "index out of bounds";
      } else {
        return this->nodes[index];
      }
    }

    tnode* get_child (int index) {
      if (index < 0 || index > size) {
        return NULL;
      } else {
        return this->children[index];
      }
    }

    static const size_t max_size = capacity;

    size_t get_size() {
      return this->size;
    }

    void set_size(size_t new_size) {
      this->size = new_size;
    }
};
