#pragma once

//template<typename T>
//struct maybe {
//  T value;
//  bool valid;
//
//  //copy constructors
//  maybe(maybe& r) {
//    this->value = r->value;
//    this->is_present = r->is_present;
//  }
//
//  maybe(T& val) {
//    this->val = val;
//  }
//
//  bool operator !() const {
//    return !valid;
//  }
//  operator bool() const {
//    return valid;
//  };
//  T& operator * () {
//    return value;
//  }
//
//};

template<typename T>
class sequence_ops{
  public:
  using Tree = T;
  using tnode = Tree;
  using ET = typename Tree::ET;
  using AT = typename Tree::AT;
  using K = typename Tree::K;
  using V = typename Tree::V;


  struct rt_node {
    public:
    tnode* first;
    tnode* second;
    ET entry_key;

   rt_node():first(NULL), second(NULL) {
   }
  };

  struct RT {
    bool first;
    rt_node second;

    RT(): first(false), second(rt_node()) {}
    RT(bool first, rt_node& second): first(first), second(second) {}
  };

  static inline tnode* make_tree_node () {
    return new Tree();
  }

  static tnode* create_root (RT r) {
    tnode* root = make_tree_node();
    root->nodes[0] = r.second.entry_key;
    root->children[0] = r.second.first;
    root->children[1] = r.second.second;
    root->set_size(1);
    root->is_leaf = false;
    root->update();
    return root;
  } 

  static RT insert_node (tnode* b, const ET& entry_key, const RT& child_tnodes, int index) {
    rt_node rt_value;
    if (b->get_size() == tnode::capacity) {
      tnode* new_node = new Tree();
      new_node->is_leaf = b->is_leaf;
      size_t mid = floor((tnode::capacity + 1)/2);
      size_t l1, r1, l2, r2;
      if (index == mid) {
        new_node->children[0] = child_tnodes.second.second;
        for (int i = mid; i < tnode::capacity; i++) {
          new_node->nodes[i-mid] = b->nodes[i];
          new_node->children[i+1-mid] = b->children[i+1];
        }
        new_node->set_size(tnode::capacity-mid);
        new_node->update();
        (b->children)[mid] = child_tnodes.second.first;
        b->set_size(mid);
        b->update();

        rt_value.entry_key = entry_key;

      } else if (index < mid) {
        new_node->children[0] = (b->children)[mid];
        for ( int i = mid; i < tnode::capacity; i++) {
          (new_node->nodes)[i-mid] = (b->nodes)[i];
          (new_node->children)[i+1-mid] = (b->children)[i+1];
        }
        new_node->set_size(tnode::capacity-mid);
        new_node->update();
        rt_value.entry_key = ((b->nodes)[mid-1]);

        for (int i = mid-2; i >= index; i--) {
          (b->nodes)[i+1] = (b->nodes)[i];
          (b->children)[i+2] = (b->children)[i+1];
        }


        (b->nodes)[index] = entry_key;
        (b->children)[index + 1] = child_tnodes.second.second;
        (b->children)[index] = child_tnodes.second.first;
        b->set_size(mid);
        b->update();
      } else {
        for (int i = mid+1; i < index; i ++) {
          (new_node->nodes)[i-(mid+1)] = (b->nodes)[i];
          (new_node->children)[i-(mid+1)] = (b->children)[i];
        }
        (new_node->children)[index - (mid+1)] = child_tnodes.second.first;
        (new_node->nodes)[index-(mid+1)] = entry_key;
        (new_node->children)[index+1-(mid+1)] = child_tnodes.second.second;  

        rt_value.entry_key = (b->nodes)[mid];

        for (int i = index; i < tnode::capacity; i++) {
          (new_node->nodes)[i+1 - (mid+1)] = (b->nodes)[i];
          (new_node->children)[i+2 - (mid+1)] = (b->children)[i+1];
        }
        (b->set_size(mid));
        b->update();
        new_node->set_size(tnode::capacity-mid);
        new_node->update();
      
      }
      rt_value.first = b;
      rt_value.second = new_node;
      return RT(true, rt_value);
    } else {
      for ( int i = b->get_size()-1; i >= index; i--) {
        (b->nodes)[i+1] = (b->nodes)[i];
        (b->children)[i+2] = (b->children)[i+1];
      }
      (b->nodes)[index] = entry_key;
      (b->children)[index+1] = child_tnodes.second.second;
      (b->children)[index] = child_tnodes.second.first;
      b->set_size(b->get_size() + 1);
      b->update();
      rt_value.first = b;
      return RT(false, rt_value);
    }
  }
};
