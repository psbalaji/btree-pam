#pragma once
#include "basic_node.h"

// *******************************************
//   AUGMENTED NODE
// *******************************************

// Creates an augmented node from a basic node.
// The new augmented entry is a pair of the original entry and the aumented
// value.   The Entry struct must have the static inteface:
//   entry_t;
//   aug_t;
//   get_empty() -> aug_t;
//   from_entry(entry_t) -> aug_t;
//   combine(aut_t, aug_t) -> aug_t;
template<class Entry>
struct aug_node : basic_node<std::pair<typename Entry::entry_t,
						typename Entry::aug_t>> {
  public:
  using AT = typename Entry::aug_t;
  using ET = typename Entry::entry_t;
  using key_t = typename Entry::key_t;
  using val_t = typename Entry::val_t;
  using basic = basic_node<std::pair<ET,AT>>;
  using node = typename basic::node;

  static ET& get_entry(node *a) {return a->entry.first;}
  static ET* get_entry_p(node *a) {return &a->entry.first;}
  static void set_entry(node *a, ET e) {a->entry.first = e;}
  static inline int compare (const key_t& k1, const key_t& k2) {
    if (comp (k1, k2)) {
      return 1;
    } else if (comp (k2, k1)) {
      return -1;
    }
    return 0;
  }
  static inline bool comp (const key_t& k1, const key_t& k2) {
    return Entry::comp(k1, k2);
  }
  static inline key_t get_key(const ET& entry) {
    return Entry::get_key(entry);
  }
  static inline AT combine (const AT& a, const AT& b) {
    return Entry::combine(a, b);
  }
  static inline AT from_entry (const ET& entry) {
    return Entry::from_entry(entry);
  }
  static inline AT get_empty () {
    return Entry::get_empty();
  }

  static AT aug_val(node* a) {
    if (a == NULL) return Entry::get_empty();
    else return (a->entry).second;}

  static node* single(ET e) {
    AT av = Entry::from_entry(e);
    return basic::single(std::make_pair(e,av));
  }
};
