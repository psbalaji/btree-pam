#include <cstdlib>
#include "pam.h"
#include <fstream>

#include<chrono>

struct int_map {
	using key_t = uint64_t;
	using val_t = uint64_t;
  using aug_t = uint64_t;

	static inline bool comp (const key_t& a, const key_t& b) {
		return a > b;
	}
  static inline aug_t from_entry (const key_t& k, const val_t& v) {
    return v;
  }
  static inline aug_t combine (const aug_t& a, const aug_t& b) {
    return a + b;
  }
  static inline aug_t get_empty () {
    return 0;
  }
};

aug_map<int_map> m;

int main () {
  using ET = aug_map<int_map>::E;
  int a,b;
  std::cin >> a;
  auto start = std::chrono::high_resolution_clock::now();
	uint64_t l = 0;
  uint64_t total = 0;
  std::ifstream f1("test_data.txt");
  std::ifstream f2("test_range.txt");
	for ( int i = 0 ; i < a; i++) {
		//l = (rand() % 1000000) + 1;
    f1 >> l;
		m.insert (std::make_pair(l, l));
    //total += (l+1);
    //std::cout<< i << " " << m.get_size() << endl << std::flush;
    //std::cout << l << " " << total << " " << m.get_total_partial_sum() << endl << std::flush;
    //std::cout << (total == m.get_total_partial_sum()) << endl << std::flush;
	}
  //std::cout<< m.get_size() << endl << std::flush;

	//std::cout << m.size() << endl; 
  //std::cout << "Hello wrold" << endl;
  //m.print();
  //std::cout << m.aug_left(0) << endl;
  auto stop  = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::cout << "insert time: " << duration.count() << endl;
  f1.seekg(0);
  start = std::chrono::high_resolution_clock::now();
  maybe<ET> result;
  for (int i = 0; i < a; i++) {
    f1 >> l;
    result = m.find(l);
    //std::cout << result.valid << std::endl;
  }
  f1.close();
  stop  = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::cout << "find time: " << duration.count() << endl;
  start = std::chrono::high_resolution_clock::now();
  int m1, n;
  for ( int i = 0; i<a && a <= 1000000; i++) {
    f2 >> m1 >> n;
    m.aug_range(n, m1);
   // std::cout << m1 << " " << n << " " << m.aug_range(n, m1) << std::endl;
  }
  f2.close();
  stop  = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop-start);
  std::cout<< "range tieme: " << duration.count() << endl;
	return 0;
}
