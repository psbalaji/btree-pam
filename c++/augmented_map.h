#pragma once

template<typename Entry>
struct aug_map_ {

  using Tree = map_ops<Entry>;
  using node = typename Tree::ET;
  using tnode = typename Tree::tnode;
  using E = typename Entry::ET;
  using K = typename Entry::key_t;
  using V = typename Entry::val_t;
  using AT = typename Entry::AT;
  using M = aug_map_;

  tnode* root;

  aug_map_(M&& m) {if (this != &m){this->root = m.root; m.root = NULL;} }
  aug_map_() { this->root = Tree::make_tree_node();}

  ~aug_map_() {    
    clear();
  }

  void clear() {
    tnode* t = root;
    __sync_bool_compare_and_swap(&(this->root), t, NULL);
  }

    // copy assignment, clear target, increment reference count, 
  M& operator = (const M& m) {
    if (this != &m) {
      tnode* tmp = root;
      clear();
      root = m.root; //GC::increment(root);
      //if (GC::initialized()) GC::decrement_recursive(tmp);
    }
    return *this;
  }

  size_t get_size () {
    return this->root->get_tree_size();
  }

  void insert (const E& p) {
    this->root = Tree::insert(this->root, p);
  }

  void print () {
    tnode* temp = this->root;
    Tree::print(temp);
  }  
  
  AT get_total_partial_sum () {
    return Tree::get_total_partial_sum(this->root);
  }

  AT aug_range (const K& k1, const K& k2) {
    Tree::aug_range (this->root, k1, k2);
  }

  AT aug_right (const K& k1) {
    Tree::aug_right(this->root, k1);
  }

  AT aug_left (const K& k1) {
    Tree::aug_left (this->root, k1);
  }

  maybe<E> find (const K& k) {
    return Tree::find(k, this->root);
  }
  tnode* get_root() {tnode* t = root; root = NULL; return t;}
};

template <class entry>
struct aug_map_full_entry : entry {
  using val_t = typename entry::val_t;
  using V = val_t;
  using key_t = typename entry::key_t;
  using K = key_t;
  using aug_t = typename entry::aug_t;
  using AT = aug_t;
  using entry_t = std::pair<key_t,val_t>;
  using ET = entry_t;
  static inline key_t get_key(const entry_t& e) {return e.first;}
  static inline val_t get_val(const entry_t& e) {return e.second;}
  static inline void set_val(entry_t& e, const val_t& v) {e.second = v;}
  static inline aug_t from_entry(const entry_t& e) {
    return entry::from_entry(e.first, e.second);}
};


template<typename T>
using aug_map = aug_map_<aug_map_full_entry<T>>;
